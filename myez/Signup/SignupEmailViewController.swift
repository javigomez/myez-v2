//
//  SignupEmailViewController.swift
//  MyEZ
//
//  Created by Javier Gomez on 9/8/17.
//  Copyright © 2017 JDev. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit

class SignupEmailViewController: UIViewController {
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var verifyPasswordText: UITextField!
    
    @IBOutlet weak var nextButton: UIButton!
    
    var isEmpty = true
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func emailChanged(_ sender: UITextField){
        if emailText.text == "" {
            self.isEmpty = true
        }
    }
    
    @IBAction func passwordChanged(_ sender: UITextField) {
        if passwordText.text == "" {
            self.isEmpty = true
        }
    }
    
    @IBAction func verifyMatchingPass(_ sender: Any) {
        if verifyPasswordText.text != passwordText.text {
            alert(message: "Passwords not matching, try again.", title: "Not matching")
            passwordText.text = ""
            verifyPasswordText.text = ""
        }
    }
    
    @IBAction func verifyPasswordChanged(_ sender: UITextField) {
        if verifyPasswordText.text == "" {
            self.isEmpty = true
        } else {
            self.isEmpty = false
            nextButton.isEnabled = true
        }
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        
        if emailText.text != "" && passwordText.text != "" {
            Auth.auth().createUser(withEmail: emailText.text!, password: passwordText.text!, completion: { (user, error) in
                if error == nil {
                    userInformation.userId = (user?.user.uid)!
                    userInformation.email = self.emailText.text!
                    
                    self.saveExtraInfoUser(userId: (user?.user.uid)!, email: self.emailText.text!)
                    
                } else {
                    if let errCode = AuthErrorCode(rawValue: error!._code) {
                        switch errCode {
                        case .invalidEmail:
                            self.alert(message: "Please enter a valid email", title: "Invalid email")
                        case .emailAlreadyInUse:
                            self.alert(message: "Email is already in use", title: "Email in use")
                        default:
                            self.alert(message: error!.localizedDescription, title: "System error")
                        }
                    }
                }
            })
        } else {
            alert(message: "Please type your email and password", title: "Missing Email/Password")
        }
    }
    
    func saveExtraInfoUser(userId: String, email: String) {
        var ref : DatabaseReference!
        ref = Database.database().reference()
        
        extraInfo.completedSigningUp = false
        
        let account = ["email": email, "completedSigningUp" : false] as [String : Any]
        
        ref.child("users").child(userId).child("account").setValue(account) {
            (error:Error?, ref:DatabaseReference) in
            if let error = error {
                print (error)
            }
        }
        
        //Go to add info
        self.performSegue(withIdentifier: "signupAddinfo", sender: self)
    }
 
    @IBAction func openTermsOnline(_ sender: UIButton) {
        
        if let url = URL(string: "https://www.ezinflatables.com/pages/terms-and-conditions") {
            UIApplication.shared.open(url, options: [:])
        }

    }
    
    func alert(message: String, title: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}


//OLD FUNCTIONS
/*
 //Save pass in keychain
 //self.savePassword(self.passwordText.text!, for: self.emailText.text!)
 
 func savePassword(_ password: String, for account: String) {
 
 print (password)
 print (account)
 
 let password = password.data(using: String.Encoding.utf8)!
 let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
 kSecAttrAccount as String: account,
 kSecValueData as String: password]
 let status = SecItemAdd(query as CFDictionary, nil)
 
 guard status == errSecSuccess else { return print("save error")}
 }
 
 //                    let preferences = UserDefaults.standard
 //                    preferences.set((user?.user.uid)!, forKey: "session")
 //                    preferences.set(self.emailText.text!, forKey: "userEmail")
 //

 
 */
